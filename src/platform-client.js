(function () {
    'use strict';

    angular.module('platform-client', []).provider('platformClientConfig', function PlatformClientConfigProvider() {
        var apiPrefix = 'https://webapi.platform.sportsdigita.com/api';
        var liveApiPrefix = 'https://live.platform.sportsdigita.com/api';
        var analyticsApiPrefix = 'https://analytics.platform.sportsdigita.com/api';
        var storageType = 'session'; //session, local

        return {
            setApiPrefix: function (value) {
                apiPrefix = value;
            },
            setLiveApiPrefix: function (value) {
                liveApiPrefix = value;
            },
            setAnalyticsApiPrefix: function (value) {
                analyticsApiPrefix = value;
            },
            setSessionStorageType: function () {
                storageType = 'session';
            },
            setLocalStorageType: function () {
                storageType = 'local';
            },
            $get: function () {
                return {
                    apiPrefix: apiPrefix,
                    liveApiPrefix: liveApiPrefix,
                    analyticsApiPrefix: analyticsApiPrefix,
                    storageType: storageType
                };
            }
        };
    });
})();
