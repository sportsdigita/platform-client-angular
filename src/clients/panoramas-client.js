(function () {
    'use strict';
    angular.module('platform-client').service('panoramaClient', [
        '$http',
        '$q',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'Upload',
        'batchRequestsService',
        function ($http, $q, config, $filter, tokenManager, Upload, batchRequestsService) {
            this.uploadAndGeneratePanorama = function (panoramaId, warpedImage, progressCallback, panoramaType) {
                var deferred = $q.defer();

                if (!warpedImage) {
                    throw new Error('warpedImage required.');
                }

                Upload.rename(warpedImage, warpedImage.name.replace(/[^a-zA-Z0-9-_\.]/g, ''));
                Upload.upload({
                    url: config.apiPrefix + '/v1/panoramas/' + panoramaId + '/sourceimage',
                    data: {
                        file: warpedImage,
                        data: JSON.stringify({ panoramaType: panoramaType })
                    },
                    headers: { Authorization: tokenManager.getAuthorizationHeader() }
                }).then(
                    function (data) {
                        deferred.resolve(data.data);
                    },
                    function (err) {
                        deferred.reject({ error: err });
                    },
                    function (evt) {
                        var progressPercentage = parseInt((100.0 * evt.loaded) / evt.total);
                        (progressCallback || angular.noop)({
                            progress: progressPercentage
                        });
                    }
                );

                return deferred.promise;
            };

            this.duplicatePanorama = function (srcPanoramaId, newPanorama) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/panoramas/' + srcPanoramaId, newPanorama, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPanoramas = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/panoramas', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPanorama = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/panoramas/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createPanorama = function (panorama) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/panoramas', panorama, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePanorama = function (id, panorama) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/panoramas/' + id, JSON.stringify(panorama), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePanorama = function (id) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/panoramas/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
