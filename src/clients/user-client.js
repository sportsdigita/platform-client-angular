(function () {
    'use strict';
    angular.module('platform-client').service('userClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getUser = function (userId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/users/' + userId, { params: filter, headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUsers = function (filter) {
                var deferred = $q.defer();
                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/users', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.validateEmail = function (email) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/useremails?email=' + email)
                    .then(function (data) {
                        deferred.resolve(data.status == 200);
                    })
                    .catch(function (response) {
                        response.status == 404 ? deferred.resolve(false) : deferred.reject({ response: response });
                    });

                return deferred.promise;
            };

            this.createUser = function (createdUser, sendEmail) {
                var deferred = $q.defer();

                if (typeof sendEmail == 'undefined') sendEmail = true;

                $http
                    .post(config.apiPrefix + '/v1/users?sendemail=' + sendEmail, createdUser)
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateUser = function (userId, user) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/users/' + userId, user, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUserDeckDetails = function (userId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/users/' + userId + '/decks', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUserOrganizationDetails = function (userId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/users/' + userId + '/organizations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
