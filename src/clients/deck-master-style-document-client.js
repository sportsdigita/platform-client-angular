(function () {
    'use strict';
    angular.module('platform-client').service('deckMasterStyleDocumentClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.getDeckMasterStyleDocuments = function (deckId, masterStyleId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/masterStyleDocuments', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckMasterStyleDocument = function (deckId, masterStyleId, masterStyleDocumentId, columns) {
                var deferred = $q.defer();

                $http
                    .get(
                        config.apiPrefix +
                            '/v1/decks/' +
                            deckId +
                            '/masterStyles/' +
                            masterStyleId +
                            '/masterStyleDocuments/' +
                            masterStyleDocumentId +
                            (columns && columns.length ? '?columns=' + columns : ''),
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createDeckMasterStyleDocument = function (deckId, masterStyleId, masterStyleDocument) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/masterStyleDocuments', masterStyleDocument, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckMasterStyleDocument = function (deckId, masterStyleId, masterStyleDocumentId, masterStyleDocument) {
                var deferred = $q.defer();

                $http
                    .put(
                        config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/masterStyleDocuments/' + masterStyleDocumentId,
                        masterStyleDocument,
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckMasterStyleDocuments = function (deckId, masterStyleId, masterStyleDocuments) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/masterStyleDocuments', masterStyleDocuments, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckMasterStyleDocument = function (deckId, masterStyleId, masterStyleDocumentId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/masterStyleDocuments/' + masterStyleDocumentId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
