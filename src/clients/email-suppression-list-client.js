(function () {
    'use strict';
    angular.module('platform-client').service('emailSuppressionListClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {

            this.deleteEmailAddressFromSuppressionList = function (emailAddress) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/emailSuppressionList/' + emailAddress, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
