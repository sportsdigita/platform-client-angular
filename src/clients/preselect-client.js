(function () {
    'use strict';
    angular.module('platform-client').service('preselectClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.getPreselects = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/preselects', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPreselect = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/preselects/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createPreselect = function (preselect) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/preselects', preselect, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePreselect = function (preselectId, preselect) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/preselects/' + preselectId, preselect, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePreselect = function (id) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/preselects/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
