(function () {
    'use strict';
    angular.module('platform-client').service('styleDocumentClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.getStyleDocuments = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/styledocuments', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getStyleDocument = function (styleDocumentId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/styledocuments/' + styleDocumentId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createStyleDocument = function (styleDocument) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/styledocuments', styleDocument, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateStyleDocument = function (styleDocumentId, styleDocument) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/styledocuments/' + styleDocumentId, styleDocument, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteStyleDocument = function (styleDocumentId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/styledocuments/' + styleDocumentId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteStyleDocuments = function (styleDocumentIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/styledocuments',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: styleDocumentIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
