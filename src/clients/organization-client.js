(function () {
    'use strict';
    angular.module('platform-client').service('organizationClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.getOrganizations = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/organizations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getOrganization = function (id, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizations/' + id, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateOrganization = function (id, organization) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/organizations/' + id, organization, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createOrganization = function (newOrganization) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/organizations/', newOrganization, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteOrganization = function (orgId) {
                var deferred = $q.defer();
                $http
                    .delete(config.apiPrefix + '/v1/organizations/' + orgId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };

            this.getOrganizationUserRoles = function (organizationId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizations/' + organizationId + '/roles', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            // User Groups

            this.getUserGroups = function (organizationId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/organizations/' + organizationId + '/usergroups', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUserGroup = function (organizationId, userGroupId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizations/' + organizationId + '/usergroups/' + userGroupId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createUserGroup = function (organizationId, userGroup) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/organizations/' + organizationId + '/usergroups', userGroup, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateUserGroup = function (organizationId, userGroupId, userGroup) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/organizations/' + organizationId + '/usergroups/' + userGroupId, userGroup, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteUserGroup = function (organizationId, userGroupId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/organizations/' + organizationId + '/usergroups/' + userGroupId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteUserGroups = function (organizationId, userGroupIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/organizations/' + organizationId + '/usergroups',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: userGroupIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.authenticateOrganizationWithGong = function (organizationId, code, redirectUri) {
                var deferred = $q.defer();

                $http
                    .post(
                        config.apiPrefix + '/v1/organizations/' + organizationId + '/gongtoken',
                        { code: code, redirectUri: redirectUri },
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.disconnectGongFromOrganization = function (organizationId) {
                var deferred = $q.defer();
                $http
                    .delete(config.apiPrefix + '/v1/organizations/' + organizationId + '/gongtoken', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };
        }
    ]);
})();
