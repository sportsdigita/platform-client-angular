(function () {
    'use strict';
    angular.module('platform-client').service('organizationComponentMigrationClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getOrganizationComponentMigration = function (organizationId, nodeName, componentMigrationId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizations/' + organizationId + '/components/' + nodeName + '/migrations/' + componentMigrationId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getOrganizationComponentMigrations = function (organizationId, nodeName, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/organizations/' + organizationId + '/components/' + nodeName + '/migrations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createOrganizationComponentMigration = function (organizationId, nodeName, componentMigration) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/organizations/' + organizationId + '/components/' + nodeName + '/migrations', componentMigration, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
