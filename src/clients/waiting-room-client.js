(function () {
    'use strict';
    angular.module('platform-client').service('waitingRoomClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getWaitingRooms = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/user/waitingRooms', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getWaitingRoom = function (waitingRoomId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/user/waitingRooms/' + waitingRoomId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createWaitingRoom = function (waitingRoom) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/user/waitingRooms', waitingRoom, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateWaitingRoom = function (waitingRoomId, waitingRoom) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/user/waitingRooms/' + waitingRoomId, waitingRoom, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteWaitingRoom = function (waitingRoomId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/user/waitingRooms/' + waitingRoomId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
