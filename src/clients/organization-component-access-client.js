(function () {
    'use strict';
    angular.module('platform-client').service('organizationComponentAccessClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getOrganizationComponentAccesses = function (organizationId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentAccesses', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getOrganizationComponentAccess = function (organizationId, componentAccessId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentAccesses/' + componentAccessId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createOrganizationComponentAccess = function (organizationId, componentAccess) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentAccesses', componentAccess, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateOrganizationComponentAccess = function (organizationId, componentAccessId, componentAccess) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentAccesses/' + componentAccessId, componentAccess, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteOrganizationComponentAccess = function (organizationId, componentAccessId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentAccesses/' + componentAccessId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
