(function () {
    'use strict';
    angular.module('platform-client').service('deckClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getDecks = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeck = function (id, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + id, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeck = function (deckId, deck) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId, JSON.stringify(deck), { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.duplicateDeck = function (deckId, newDeck) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId, JSON.stringify(newDeck), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeck = function (deckId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckChapters = function (deckId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/chapters', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckChapterSlides = function (deckId, chapterId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/chapters/' + chapterId + '/slides', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckSlides = function (id, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + id + '/slides', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckSlide = function (id, slideId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + id + '/slides/' + slideId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() },
                        params: filter
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createDeckSlidePreviewImage = function (deckId, slideId, forceRegeneration) {
                var deferred = $q.defer();

                $http
                    .post(
                        config.apiPrefix + '/v1/decks/' + deckId + '/slides/' + slideId + '/previewImage?force=' + (forceRegeneration || false),
                        {},
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createDeckSlidePreviewImages = function (id, slideIds, forceRegeneration) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + id + '/slidepreviews?force=' + (forceRegeneration || false), slideIds, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.addDeckSlidesToDeck = function (id, slides) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + id + '/slides', JSON.stringify(slides), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckSlide = function (deckId, slideId, slide, globalUpdate) {
                var deferred = $q.defer();
                var globalString = globalUpdate && globalUpdate === true ? '?globalUpdate=true' : '';

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/slides/' + slideId + globalString, JSON.stringify(slide), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.replaceDeckSlide = function (deckId, slideId, slideReplacement) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/slides/' + slideId + '?action=replace', JSON.stringify(slideReplacement), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckSlides = function (deckId, slides) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/slides', JSON.stringify(slides), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckSlide = function (deckId, slideId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/slides/' + slideId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckSlides = function (deckId, slideIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/slides',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: slideIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckUserRoles = function (deckId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/roles', { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            // Master Styles

            this.getMasterStyles = function (deckId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles', {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getMasterStyle = function (deckId, masterStyleId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getMasterStyleAssociations = function (deckId, masterStyleId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/associations', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createMasterStyle = function (deckId, masterStyle) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles', masterStyle, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateMasterStyle = function (deckId, masterStyleId, masterStyle) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId, masterStyle, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteMasterStyle = function (deckId, masterStyleId) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: [masterStyleId],
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteMasterStyles = function (deckId, masterStyleIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: masterStyleIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.duplicateMasterStyle = function (deckId, masterStyleId, masterStyleOverrides) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId, masterStyleOverrides, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            // Master Style Forms

            this.getDeckMasterStyleForm = function (deckId, masterStyleId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/form', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createDeckMasterStyleForm = function (deckId, masterStyleId, deckMasterStyleForm) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/form', deckMasterStyleForm, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckMasterStyleForm = function (deckId, masterStyleId, deckMasterStyleFormId, deckMasterStyleForm) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/form/' + deckMasterStyleFormId, deckMasterStyleForm, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckMasterStyleForm = function (deckId, masterStyleId, deckMasterStyleFormId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/masterStyles/' + masterStyleId + '/form/' + deckMasterStyleFormId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUserActivityNotification = function (deckId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/activitynotification', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createUserActivityNotification = function (deckId, notification) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/activitynotification', notification, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateUserActivityNotification = function (deckId, notification) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/activitynotification', notification, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteUserActivityNotification = function (deckId) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/activitynotification',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyComponentStyleToAllDeckComponents = function (deckId, masterStyleId, componentStyleId, options = {}) {
                var deferred = $q.defer();
                $http
                    .post(config.apiPrefix + `/v1/decks/${deckId}/masterstyles/${masterStyleId}/componentstyles/${componentStyleId}/applyAll`, options, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getApplyComponentStyleToAllDeckComponentsJobStatus = function (deckId, masterStyleId, componentStyleId, jobId) {
                var deferred = $q.defer();
                $http
                    .get(config.apiPrefix + `/v1/decks/${deckId}/masterstyles/${masterStyleId}/componentstyles/${componentStyleId}/applyAll/status/${jobId}`, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.globalUpdateGlobalDataStoreDataManagement = function (deckId) {
                var deferred = $q.defer();
                $http
                    .post(
                        config.apiPrefix + `/v1/decks/${deckId}/globalDataStoreDataManagement/globalUpdate`,
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
