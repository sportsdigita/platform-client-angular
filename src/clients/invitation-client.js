(function () {
    'use strict';
    angular.module('platform-client').service('invitationClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.sendInvitations = function (invitations) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/invitations', JSON.stringify(invitations), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getInvitations = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/invitations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.completeInvitation = function (invitation) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/callbacks/invitationCompleted', JSON.stringify(invitation), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getInvitation = function (id, filter) {
                var deferred = $q.defer();

                var body = {
                    params: filter
                };

                if (tokenManager.getToken()) body.headers = { Authorization: tokenManager.getAuthorizationHeader() };

                $http
                    .get(config.apiPrefix + '/v1/invitations/' + id, body)
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteInvitations = function (ids) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/invitations',
                    method: 'DELETE',
                    data: ids,
                    headers: { 'Content-Type': 'application/json;charset=utf-8', Authorization: tokenManager.getAuthorizationHeader() }
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
