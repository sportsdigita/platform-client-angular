(function () {
    'use strict';
    angular.module('platform-client').service('deckRulesetClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getRulesets = function (deckId, filter) {
                var deferred = $q.defer();
                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };

            this.getRuleset = function (deckId, rulesetId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets/' + rulesetId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };

            this.createRuleset = function (deckId, ruleset) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets', ruleset, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateRuleset = function (deckId, rulesetId, ruleset) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets/' + rulesetId, ruleset, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };

            this.deleteRuleset = function (deckId, rulesetId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets/' + rulesetId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };

            this.deleteRulesets = function (deckId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets', { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };
        }
    ]);
})();
