(function () {
    'use strict';
    angular.module('platform-client').service('rulesetClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.evaluateFacts = function (rulesetId, facts) {
                var deferred = $q.defer();
                $http
                    .post(config.apiPrefix + '/v1/rulesets/' + rulesetId + '/evaluation', facts, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };
        }
    ]);
})();
