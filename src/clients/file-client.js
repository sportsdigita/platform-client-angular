(function () {
    'use strict';
    angular.module('platform-client').service('fileClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getFiles = function (filter) {
                var deferred = $q.defer();

                var fileFilter = filter.filter || filter;

                if (filter.columns) {
                    fileFilter.columns = filter.columns;
                }

                $http
                    .post(config.apiPrefix + '/v1/files/list', fileFilter, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateFile = function (file) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/files/', file, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteFiles = function (fileIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/files',
                    method: 'DELETE',
                    data: fileIds,
                    headers: { 'Content-Type': 'application/json;charset=utf-8', Authorization: tokenManager.getAuthorizationHeader() }
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
