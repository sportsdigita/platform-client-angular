(function () {
    'use strict';

    angular.module('platform-client').service('authenticateClient', [
        '$http',
        '$window',
        '$q',
        '$timeout',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $window, $q, $timeout, config, tokenManager) {
            this.signIn = function (account) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/authentication', account)
                    .then(function (data) {
                        tokenManager.setToken(data.data.token);
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateUser = function (updateUser) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/authentication/user', updateUser, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.changePassword = function (passwords) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/authentication/changePassword', passwords, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.changePasswordForUser = function (userId, passwords) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/authentication/user/password', {requestedUserId: userId, passwords: passwords}, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.signOut = function (success, error) {
                var deferred = $q.defer();

                $timeout(function () {
                    tokenManager.clearToken();
                    deferred.resolve();
                }, 100);

                return deferred.promise;
            };

            this.getSamlLoginForSubdomain = function (subdomain) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/authentication/saml/' + subdomain + '/login', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getSamlLoginsForEmail = function (email) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/authentication/saml/organizationusers/' + email + '/login', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUserInfo = function (success, error) {
                return $http.get(config.apiPrefix + '/v1/authentication/user', { headers: { Authorization: tokenManager.getAuthorizationHeader() } });
            };

            this.generateApiKey = function () {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/authentication/apikey', {}, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.generateApiKeyForUser = function (userId) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/authentication/user/apikey', {userId: userId}, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.forgotPassword = function (email) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/authentication/forgotPassword', { email: email })
                    .then(function () {
                        deferred.resolve();
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.resetPassword = function (email, password, token) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/authentication/resetPassword', { email: email, password: password, token: token })
                    .then(function (data) {
                        tokenManager.setToken(data.data.token);
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.validateResetPasswordToken = function (email, token) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/authentication/resetPassword/validtoken', { email: email, token: token })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
