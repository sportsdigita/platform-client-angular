(function () {
    'use strict';
    angular.module('platform-client').service('presentationViewClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            this.getPresentationViewData = function (subdomain, presentationName, vanityUrl) {
                var deferred = $q.defer();
                var url = config.apiPrefix + '/v1/presentationview';
                var args = [
                    { name: 'subdomain', value: subdomain },
                    { name: 'presentationName', value: presentationName },
                    { name: 'vanityUrl', value: vanityUrl }
                ];
                var paramsOnUrl = false;
                args.forEach(function (arg) {
                    var paramChar = paramsOnUrl ? '&' : '?';
                    if (arg && arg.name && arg.value && arg.value.length) {
                        url += paramChar + arg.name + '=' + arg.value;
                        paramsOnUrl = true;
                    }
                });

                $http
                    .get(url, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
