(function () {
    'use strict';
    angular.module('platform-client').service('fontClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'Upload',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, Upload, batchRequestsService) {
            this.uploadWoff = function (deckId, fontId, woffFile, progressCallback) {
                var deferred = $q.defer();

                if (!woffFile) {
                    throw new Error('woffFile required.');
                }

                Upload.rename(woffFile, woffFile.name.replace(/[^a-zA-Z0-9-_\.]/g, ''));
                Upload.upload({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/fonts/' + fontId + '/woff',
                    data: {
                        file: woffFile
                    },
                    headers: { Authorization: tokenManager.getAuthorizationHeader() }
                }).then(
                    function (data) {
                        deferred.resolve(data.data);
                    },
                    function (err) {
                        deferred.reject({ error: err });
                    },
                    function (evt) {
                        var progressPercentage = parseInt((100.0 * evt.loaded) / evt.total);
                        (progressCallback || angular.noop)({
                            progress: progressPercentage
                        });
                    }
                );

                return deferred.promise;
            };

            this.uploadTtf = function (deckId, fontId, ttfFile, progressCallback) {
                var deferred = $q.defer();

                if (!ttfFile) {
                    throw new Error('ttfFile required.');
                }

                Upload.rename(ttfFile, ttfFile.name.replace(/[^a-zA-Z0-9-_\.]/g, ''));
                Upload.upload({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/fonts/' + fontId + '/ttf',
                    data: {
                        file: ttfFile
                    },
                    headers: { Authorization: tokenManager.getAuthorizationHeader() }
                }).then(
                    function (data) {
                        deferred.resolve(data.data);
                    },
                    function (err) {
                        deferred.reject({ error: err });
                    },
                    function (evt) {
                        var progressPercentage = parseInt((100.0 * evt.loaded) / evt.total);
                        (progressCallback || angular.noop)({
                            progress: progressPercentage
                        });
                    }
                );

                return deferred.promise;
            };

            this.getFonts = function (deckId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/fonts', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getFont = function (deckId, fontId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/fonts/' + fontId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createFont = function (deckId, font) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/fonts', font, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateFont = function (deckId, fontId, font) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/fonts/' + fontId, font, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteFont = function (deckId, fontId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/fonts/' + fontId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
