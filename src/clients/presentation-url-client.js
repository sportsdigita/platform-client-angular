(function () {
    'use strict';
    angular.module('platform-client').service('presentationUrlClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getUrls = function (presentationId, filter) {
                var deferred = $q.defer();
                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/presentations/' + presentationId + '/urls', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUrl = function (presentationId, urlId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + presentationId + '/urls/' + urlId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.addUrl = function (presentationId, url) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/presentations/' + presentationId + '/urls', url, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateUrl = function (presentationId, urlId, url) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/presentations/' + presentationId + '/urls/' + urlId, url, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteUrl = function (presentationId, urlId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/presentations/' + presentationId + '/urls/' + urlId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
