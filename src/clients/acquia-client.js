(function () {
    'use strict';
    angular.module('platform-client').service('acquiaClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.getAcquiaClientOauthData = function () {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/acquia/oauth', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
