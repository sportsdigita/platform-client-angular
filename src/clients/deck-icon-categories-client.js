(function () {
    'use strict';
    angular.module('platform-client').service('deckIconCategoriesClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        'clientUtil',
        function ($http, $q, config, tokenManager, batchRequestsService, clientUtil) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getDeckIconCategories = function (deckId, filter, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                batchRequestsService
                    .batchGetRequests(apiPrefix + '/v1/decks/' + deckId + '/iconcategories', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckIconCategory = function (deckId, iconCategoryId, filter, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                $http
                    .get(apiPrefix + '/v1/decks/' + deckId + '/iconcategories/' + iconCategoryId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
