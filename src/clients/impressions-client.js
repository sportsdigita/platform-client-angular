(function () {
    'use strict';
    angular.module('platform-client').service('impressionClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, tokenManager, batchRequestsService) {
            this.getImpressions = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/impressions', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getImpressionStats = function (groupBy, groupType, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.analyticsApiPrefix + '/v1/impressionstats/' + groupBy + '/' + groupType, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
