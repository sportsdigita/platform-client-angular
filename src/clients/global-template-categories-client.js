(function () {
    'use strict';
    angular.module('platform-client').service('globalTemplateCategoriesClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getGlobalTemplateCategories = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/globaltemplatecategories', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getGlobalTemplateCategory = function (globalTemplateCategoryId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/globaltemplatecategories/' + globalTemplateCategoryId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createGlobalTemplateCategory = function (globalTemplateCategory) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/globaltemplatecategories', globalTemplateCategory, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateGlobalTemplateCategory = function (globalTemplateCategoryId, globalTemplateCategory) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/globaltemplatecategories/' + globalTemplateCategoryId, globalTemplateCategory, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteGlobalTemplateCategory = function (globalTemplateCategoryId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/globaltemplatecategories/' + globalTemplateCategoryId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
