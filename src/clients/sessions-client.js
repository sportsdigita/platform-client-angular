(function () {
    'use strict';
    angular.module('platform-client').service('sessionsClient', [
        '$q',
        'platformClientConfig',
        'batchRequestsService',
        function ($q, config, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getSessions = function (deckId, presenationId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presenationId + '/sessions', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (err, status) {
                        deferred.reject({ response: err, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
