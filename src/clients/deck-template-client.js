(function () {
    'use strict';
    angular.module('platform-client').service('deckTemplateClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.getTemplates = function (deckId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/templates', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getTemplate = function (deckId, templateId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/templates/' + templateId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createTemplate = function (deckId, template) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/templates', template, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateTemplate = function (deckId, templateId, template) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/templates/' + templateId, template, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateTemplates = function (deckId, templates) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/templates', templates, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteTemplate = function (deckId, templateId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/templates/' + templateId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteTemplates = function (deckId, templateIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/templates',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: templateIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            var getPersistComponentsCustomContentString = function (persistComponentsCustomContent) {
                return persistComponentsCustomContent && persistComponentsCustomContent === true ? '?persistComponentsCustomContent=true' : '';
            };

            this.applyTemplateToDeckSlide = function (deckId, templateId, slideId, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix + '/v1/decks/' + deckId + '/templates/' + templateId + '/slides/' + slideId + persistComponentsCustomContentString,
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyTemplateToDeckSlides = function (deckId, templateId, slideIds, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/templates/' + templateId + '/slides' + persistComponentsCustomContentString, slideIds, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyTemplateToPresentationSlide = function (deckId, templateId, presentationId, slideId, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix +
                            '/v1/decks/' +
                            deckId +
                            '/templates/' +
                            templateId +
                            '/presentations/' +
                            presentationId +
                            '/slides/' +
                            slideId +
                            persistComponentsCustomContentString,
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyTemplateToPresentationSlides = function (deckId, templateId, presentationId, slideIds, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix +
                            '/v1/decks/' +
                            deckId +
                            '/templates/' +
                            templateId +
                            '/presentations/' +
                            presentationId +
                            '/slides' +
                            persistComponentsCustomContentString,
                        slideIds,
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
