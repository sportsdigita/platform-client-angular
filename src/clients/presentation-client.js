(function () {
    'use strict';
    angular.module('platform-client').service('presentationClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getPresentations = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/presentations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentation = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getZipStatus = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + id + '/zip/status', { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationChapters = function (presentationId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/presentations/' + presentationId + '/chapters', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationChapterSlides = function (presentationId, chapterId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/presentations/' + presentationId + '/chapters/' + chapterId + '/slides', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationSlides = function (id, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/presentations/' + id + '/slides', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationSlide = function (id, slideId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + id + '/slides/' + slideId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.validateName = function (name, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentationnames/?name=' + name, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.addPresentation = function (presentation) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/presentations', JSON.stringify(presentation), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePresentation = function (presentationId, presentation) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/presentations/' + presentationId, presentation, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePresentations = function (presentations) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/presentations', presentations, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePresentation = function (presentationId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/presentations/' + presentationId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePresentations = function (presentationIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/presentations',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: JSON.stringify(presentationIds),
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.addPresentationSlidesToPresentation = function (id, slides) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/presentations/' + id + '/slides', JSON.stringify(slides), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.generateZip = function (presentationId) {
                var deferred = $q.defer();

                $http
                    .post(
                        config.apiPrefix + '/v1/presentations/' + presentationId + '/zip',
                        {},
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.generateZipExport = function (presentationId) {
                var deferred = $q.defer();

                $http
                    .post(
                        config.apiPrefix + '/v2/presentations/' + presentationId + '/zip',
                        {},
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.generateSlidePreviewImages = function (presentationId, presentationSlideIds, forceRegeneration) {
                var deferred = $q.defer();

                $http
                    .post(
                        config.apiPrefix + '/v1/presentations/' + presentationId + '/slidepreviews?force=' + (forceRegeneration || false),
                        presentationSlideIds,
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePresentationSlide = function (presentationId, slideId, slide) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/presentations/' + presentationId + '/slides/' + slideId, JSON.stringify(slide), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.replacePresentationSlide = function (presentationId, slideId, slideReplacement) {
                var deferred = $q.defer();

                $http
                    .put(
                        config.apiPrefix + '/v1/presentations/' + presentationId + '/slides/' + slideId + '?action=replace',
                        JSON.stringify(slideReplacement),
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePresentationSlides = function (presentationId, slides) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/presentations/' + presentationId + '/slides', JSON.stringify(slides), {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePresentationSlide = function (presentationId, slideId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/presentations/' + presentationId + '/slides/' + slideId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePresentationSlides = function (presentationId, slideIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/presentations/' + presentationId + '/slides',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: slideIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPdfStatus = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + id + '/pdf/status', { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.generatePdf = function (presentationId, options) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/presentations/' + presentationId + '/pdf', options || {}, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationStats = function (groupBy, groupType, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.analyticsApiPrefix + '/v1/presentationstats/' + groupType + '/' + groupBy, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.generatePresentationExport = function (id) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/presentations/' + id + '/export', null, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationSlideRestrictions = function (presentationId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + presentationId + '/sliderestrictions', {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyComponentStyleToAllPresentationComponents = function (presentationId, masterStyleId, componentStyleId, options = {}) {
                var deferred = $q.defer();
                $http
                    .post(
                        config.apiPrefix + `/v1/presentations/${presentationId}/masterstyles/${masterStyleId}/componentstyles/${componentStyleId}/applyAll`,
                        options,
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getApplyComponentStyleToAllPresentationComponentsJobStatus = function (presentationId, masterStyleId, componentStyleId, jobId) {
                var deferred = $q.defer();
                $http
                    .get(
                        config.apiPrefix +
                            `/v1/presentations/${presentationId}/masterstyles/${masterStyleId}/componentstyles/${componentStyleId}/applyAll/status/${jobId}`,
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
