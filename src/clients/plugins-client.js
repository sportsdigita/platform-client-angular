(function () {
    'use strict';
    angular.module('platform-client').service('pluginsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.getPlugins = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/plugins', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPlugin = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/plugins/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createPlugin = function (plugin) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/plugins', plugin, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePlugin = function (id, plugin) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/plugins/' + id, plugin, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePlugin = function (id) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/plugins/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
