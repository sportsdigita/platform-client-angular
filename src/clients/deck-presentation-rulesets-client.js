(function () {
    'use strict';
    angular.module('platform-client').service('deckPresentationRulesetsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            this.createPresentationFromRuleSet = function (deckId, presentation, ruleSetInputs, slideMappingData) {
                var postBody = {
                    presentation: presentation,
                    ruleSetInputs: ruleSetInputs,
                    slideMappingData: slideMappingData
                };
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets/presentations', postBody, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createEntitiesFromRuleSetActions = function (deckId, ruleSetActions) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/rulesets/entities-from-rule-set-actions', ruleSetActions, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });
                return deferred.promise;
            };
        }
    ]);
})();
