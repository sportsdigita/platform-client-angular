(function () {
    'use strict';
    angular.module('platform-client').service('deckUserPreferenceClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getDeckUserPreferences = function (deckId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/deckuserpreferences', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckUserPreference = function (deckId, id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/deckuserpreferences/' + id, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createDeckUserPreference = function (deckId, deckUserPreference) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/deckuserpreferences', deckUserPreference, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createDeckUserPreferenceWithGongSettings = function (deckId, gongSettings) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/deckuserpreferences/gongsettings', gongSettings, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckUserPreferenceWithGongSettings = function (deckId, id, gongSettings) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/deckuserpreferences/' + id + '/gongsettings', gongSettings, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckUserPreference = function (deckId, id, deckUserPreference) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/deckuserpreferences/' + id, deckUserPreference, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckUserPreference = function (deckId, id) {
                var deferred = $q.defer();

                $http({
                    url: '/v1/decks/' + deckId + '/deckuserpreferences/' + id,
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json;charset=utf-8', Authorization: tokenManager.getAuthorizationHeader() }
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
