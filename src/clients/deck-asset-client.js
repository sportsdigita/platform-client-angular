(function () {
    'use strict';
    angular.module('platform-client').service('deckAssetClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'Upload',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, Upload, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getDeckAssets = function (deckId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/assets', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getDeckAsset = function (deckId, id, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/assets/' + id, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckAsset = function (deckId, id) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/assets/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteDeckAssets = function (deckId, assetIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/assets',
                    method: 'DELETE',
                    data: assetIds,
                    headers: { 'Content-Type': 'application/json;charset=utf-8', Authorization: tokenManager.getAuthorizationHeader() }
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.uploadDeckAsset = function (deckId, assetData, assetFile, progressCallback) {
                var deferred = $q.defer();

                if (!assetFile) {
                    throw new Error('assetFile required.');
                }

                Upload.rename(assetFile, assetFile.name.replace(/[^a-zA-Z0-9-_\.]/g, ''));
                Upload.upload({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/assets',
                    data: {
                        file: assetFile,
                        data: assetData
                    },
                    headers: { Authorization: tokenManager.getAuthorizationHeader() }
                }).then(
                    function (data) {
                        deferred.resolve(data.data);
                    },
                    function (err) {
                        deferred.reject({ error: err });
                    },
                    function (evt) {
                        var progressPercentage = parseInt((100.0 * evt.loaded) / evt.total);
                        (progressCallback || angular.noop)({
                            progress: progressPercentage
                        });
                    }
                );

                return deferred.promise;
            };

            this.updateDeckAsset = function (deckId, assetId, asset) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/assets/' + assetId, asset, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateDeckAssets = function (deckId, assets) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/assets', assets, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
