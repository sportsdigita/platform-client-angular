(function () {
    'use strict';
    angular.module('platform-client').service('insightConfigurationClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.getInsightConfigurations = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/insightsConfigurations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getInsightConfiguration = function (insightConfigId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/insightsConfigurations/' + insightConfigId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createInsightConfiguration = function (insightConfig) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/insightsConfigurations/', insightConfig, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateInsightConfiguration = function (insightConfigId, insightConfig) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/insightsConfigurations/' + insightConfigId, insightConfig, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteInsightConfiguration = function (insightConfigId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/insightsConfigurations/' + insightConfigId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
