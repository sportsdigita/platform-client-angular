(function () {
    'use strict';
    angular.module('platform-client').service('iconCategoriesClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        'clientUtil',
        function ($http, $q, config, tokenManager, batchRequestsService, clientUtil) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getIconCategories = function (filter, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                batchRequestsService
                    .batchGetRequests(apiPrefix + '/v1/iconcategories', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getIconCategory = function (iconCategoryId, filter, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                $http
                    .get(apiPrefix + '/v1/iconcategories/' + iconCategoryId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createIconCategory = function (icon) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/iconcategories', icon, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateIconCategory = function (iconCategoryId, icon) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/iconcategories/' + iconCategoryId, icon, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteIconCategory = function (iconCategoryId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/iconcategories/' + iconCategoryId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
