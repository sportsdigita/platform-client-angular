(function () {
    'use strict';
    angular
        .module('platform-client')
        .constant('storageType', {
            session: angular.injector(['ng']).get('$window').sessionStorage,
            local: angular.injector(['ng']).get('$window').localStorage,
            memory: angular.injector(['platform-client']).get('memoryStorageFactory')
        })
        .factory('platformClientTokenManager', [
            '$window',
            'platformClientConfig',
            'storageType',
            function ($window, platformClientConfig, storageType) {
                var getStorage = function () {
                    return storageType[platformClientConfig.storageType] || $window.localStorage;
                };

                return {
                    setToken: function (token) {
                        getStorage().setItem('platform-token', token);
                    },

                    getToken: function () {
                        return getStorage().getItem('platform-token');
                    },

                    getAuthorizationHeader: function () {
                        return 'Bearer ' + getStorage().getItem('platform-token');
                    },

                    clearToken: function () {
                        getStorage().removeItem('platform-token');
                    }
                };
            }
        ]);
})();
