(function () {
    'use strict';
    angular.module('platform-client').service('insightSummaryJobClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            this.getInsightsSummaryJobForInsightsConfiguration = function (insightConfigId, insightJobId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/insightsConfiguration/' + insightConfigId + '/insightssummaryjob/' + insightJobId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createInsightsSummaryJobForInsightsConfiguration = function (insightConfigId, jobOverrides) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/insightsConfiguration/' + insightConfigId + '/insightssummaryjob', jobOverrides, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
