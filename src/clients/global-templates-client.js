(function () {
    'use strict';
    angular.module('platform-client').service('globalTemplatesClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getGlobalTemplates = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/globaltemplates', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getGlobalTemplate = function (globalTemplateId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/globaltemplates/' + globalTemplateId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createGlobalTemplate = function (globalTemplate) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/globaltemplates', globalTemplate, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateGlobalTemplate = function (globalTemplateId, globalTemplate) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/globaltemplates/' + globalTemplateId, globalTemplate, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteGlobalTemplate = function (globalTemplateId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/globaltemplates/' + globalTemplateId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            var getPersistComponentsCustomContentString = function (persistComponentsCustomContent) {
                return persistComponentsCustomContent && persistComponentsCustomContent === true ? '?persistComponentsCustomContent=true' : '';
            };

            this.applyGlobalTemplateToDeckSlide = function (globalTemplateId, deckId, slideId, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix +
                            '/v1/globaltemplates/' +
                            globalTemplateId +
                            '/decks/' +
                            deckId +
                            '/slides/' +
                            slideId +
                            persistComponentsCustomContentString,
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyGlobalTemplateToDeckSlides = function (globalTemplateId, deckId, slideIds, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix + '/v1/globaltemplates/' + globalTemplateId + '/decks/' + deckId + '/slides' + persistComponentsCustomContentString,
                        slideIds,
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyGlobalTemplateToPresentationSlide = function (globalTemplateId, presentationId, slideId, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix +
                            '/v1/globaltemplates/' +
                            globalTemplateId +
                            '/presentations/' +
                            presentationId +
                            '/slides/' +
                            slideId +
                            persistComponentsCustomContentString,
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.applyGlobalTemplateToPresentationSlides = function (globalTemplateId, presentationId, slideIds, persistComponentsCustomContent) {
                var deferred = $q.defer();
                var persistComponentsCustomContentString = getPersistComponentsCustomContentString(persistComponentsCustomContent);

                $http
                    .put(
                        config.apiPrefix +
                            '/v1/globaltemplates/' +
                            globalTemplateId +
                            '/presentations/' +
                            presentationId +
                            '/slides' +
                            persistComponentsCustomContentString,
                        slideIds,
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
