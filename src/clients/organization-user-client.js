(function () {
    'use strict';
    angular.module('platform-client').service('organizationUserClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.getOrganizationUsers = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/organizationusers', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getOrganizationUser = function (id) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizationusers/' + id, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createOrganizationUsers = function (organizationUsers) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/organizationusers', organizationUsers, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateOrganizationUsers = function (organizationUsers) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/organizationusers', organizationUsers, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateOrganizationUser = function (orgUserId, orgUser) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/organizationusers/' + orgUserId, orgUser, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteOrganizationUsers = function (ids) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/organizationusers',
                    method: 'DELETE',
                    data: ids,
                    headers: { 'Content-Type': 'application/json;charset=utf-8', Authorization: tokenManager.getAuthorizationHeader() }
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
