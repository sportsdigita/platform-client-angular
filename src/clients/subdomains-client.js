(function () {
    'use strict';
    angular.module('platform-client').service('subdomainClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            this.validateSubdomain = function (name) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/subdomains/unique/' + name, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
