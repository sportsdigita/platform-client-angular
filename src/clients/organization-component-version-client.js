(function () {
    'use strict';
    angular.module('platform-client').service('organizationComponentVersionsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getOrganizationComponentVersion = function (organizationId, componentVersionId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentversions/' + componentVersionId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getOrganizationComponentVersions = function (organizationId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentversions', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateOrganizationComponentVersion = function (organizationId, componentVersionId, componentVersion) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentversions/' + componentVersionId, componentVersion, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteOrganizationComponentVersion = function (organizationId, componentVersionId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/organizations/' + organizationId + '/componentversions/' + componentVersionId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
