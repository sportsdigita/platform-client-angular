(function () {
    'use strict';
    angular.module('platform-client').service('presentationSlideAuditClient', [
        '$http',
        '$q',
        'platformClientTokenManager',
        'batchRequestsService',
        'clientUtil',
        function ($http, $q, tokenManager, batchRequestsService, clientUtil) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getPresentationSlideAudits = function (presentationId, slideId, query, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                batchRequestsService
                    .batchGetRequests(apiPrefix + '/v1/presentations/' + presentationId + '/slides/' + slideId + '/audits', query)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationSlideAudit = function (presentationId, slideId, id, query, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                $http
                    .get(apiPrefix + '/v1/presentations/' + presentationId + '/slides/' + slideId + '/audits/' + id, {
                        params: query,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationSlideAuditUsers = function (presentationId, slideId, query, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                batchRequestsService
                    .batchGetRequests(apiPrefix + '/v1/presentations/' + presentationId + '/slides/' + slideId + '/auditusers', query)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
