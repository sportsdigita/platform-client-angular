(function () {
    'use strict';
    angular.module('platform-client').service('openAiClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            this.createChatCompletion = function (sessionId, data) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/openai/chat/' + sessionId + '/completions', data, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
