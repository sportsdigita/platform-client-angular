(function () {
    'use strict';
    angular.module('platform-client').service('presentationComponentVersionsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getPresentationComponentRegistry = function (presentationId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + presentationId + '/components/registry', {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationComponentVersion = function (presentationId, componentVersionId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/presentations/' + presentationId + '/componentVersions/' + componentVersionId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationComponentVersions = function (presentationId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/presentations/' + presentationId + '/componentVersions', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.addComponentsToPresentationComponentRegistry = function (presentationId, components, filter) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/presentations/' + presentationId + '/components/registry', components, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteComponentFromPresentationRegistry = function (presentationId, componentVersionId, filter) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/presentations/' + presentationId + '/components/registry/' + componentVersionId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePresentationRegistryComponentVersionToLatest = function (presentationId, componentId) {
                var deferred = $q.defer();

                $http
                    .put(
                        config.apiPrefix + '/v1/presentations/' + presentationId + '/components/registry/' + componentId + '/latest',
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateAllPresentationRegistryComponentVersionToLatest = function (presentationId) {
                var deferred = $q.defer();

                $http
                    .put(
                        config.apiPrefix + '/v1/presentations/' + presentationId + '/components/registry/latest',
                        {},
                        {
                            headers: { Authorization: tokenManager.getAuthorizationHeader() }
                        }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
