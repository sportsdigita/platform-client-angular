(function () {
    'use strict';
    angular.module('platform-client').service('iconsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        'clientUtil',
        function ($http, $q, config, tokenManager, batchRequestsService, clientUtil) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getIcons = function (filter, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                batchRequestsService
                    .batchGetRequests(apiPrefix + '/v1/icons', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getIcon = function (iconId, filter, options) {
                var deferred = $q.defer();
                var apiPrefix = clientUtil.getApiPrefix(options);

                $http
                    .get(apiPrefix + '/v1/icons/' + iconId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createIcon = function (icon) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/icons', icon, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateIcon = function (iconId, icon) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/icons/' + iconId, icon, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteIcon = function (iconId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/icons/' + iconId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
