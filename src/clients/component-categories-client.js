(function () {
    'use strict';
    angular.module('platform-client').service('componentCategoriesClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getComponentCategories = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/componentCategories', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getComponentCategory = function (componentCategoryId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/componentCategories/' + componentCategoryId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createComponentCategory = function (componentCategory) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/componentCategories', componentCategory, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateComponentCategory = function (componentCategoryId, componentCategory) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/componentCategories/' + componentCategoryId, componentCategory, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteComponentCategory = function (componentCategoryId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/componentCategories/' + componentCategoryId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
