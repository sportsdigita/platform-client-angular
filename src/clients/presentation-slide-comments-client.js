(function () {
    'use strict';
    angular.module('platform-client').service('presentationSlideCommentsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getPresentationSlideComments = function (deckId, presentationId, slideId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(
                        config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presentationId + '/slides/' + slideId + '/comments',
                        filter
                    )
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getPresentationSlideComment = function (deckId, presentationId, slideId, id, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presentationId + '/slides/' + slideId + '/comments/' + id, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createPresentationSlideComment = function (deckId, presentationId, slideId, comment) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presentationId + '/slides/' + slideId + '/comments', comment, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updatePresentationSlideComment = function (deckId, presentationId, slideId, id, comment) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presentationId + '/slides/' + slideId + '/comments/' + id, comment, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.resolvePresentationSlideComment = function (deckId, presentationId, slideId, id) {
                var deferred = $q.defer();

                $http
                    .put(
                        config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presentationId + '/slides/' + slideId + '/comments/' + id + '/resolve',
                        {},
                        { headers: { Authorization: tokenManager.getAuthorizationHeader() } }
                    )
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deletePresentationSlideComment = function (deckId, presentationId, slideId, id) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/presentations/' + presentationId + '/slides/' + slideId + '/comments/' + id,
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json;charset=utf-8', Authorization: tokenManager.getAuthorizationHeader() }
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
