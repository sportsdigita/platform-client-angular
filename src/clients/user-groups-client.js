(function () {
    'use strict';
    angular.module('platform-client').service('userGroupsClient', [
        '$http',
        '$q',
        '$timeout',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, $timeout, config, $filter, tokenManager, batchRequestsService) {
            this.getUserGroups = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/userGroups', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getUserGroup = function (userGroupId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/userGroups/' + userGroupId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createUserGroup = function (userGroup) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/userGroups', userGroup, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateUserGroup = function (userGroupId, userGroup) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/userGroups/' + userGroupId, userGroup, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteUserGroup = function (userGroupId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/usergroups/' + userGroupId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteUserGroups = function (userGroupIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/userGroups',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: userGroupIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
