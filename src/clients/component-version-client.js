(function () {
    'use strict';
    angular.module('platform-client').service('componentVersionClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getComponentVersions = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/componentVersions', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getComponentVersion = function (componentVersionId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/componentVersions/' + componentVersionId, {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createComponentVersion = function (componentVersion) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/componentVersions', componentVersion, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateComponentVersion = function (componentVersionId, componentVersion) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/componentVersions/' + componentVersionId, componentVersion, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteComponentVersion = function (componentVersionId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/componentVersions/' + componentVersionId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
