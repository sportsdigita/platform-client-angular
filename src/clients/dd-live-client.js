(function () {
    'use strict';
    angular.module('platform-client').service('ddLiveClient', [
        '$q',
        'platformClientConfig',
        'batchRequestsService',
        function ($q, config, batchRequestsService) {
            this.cancelRequests = batchRequestsService.cancelRequests;

            this.getOpenPresentations = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.liveApiPrefix + '/v1/openpresentations', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
