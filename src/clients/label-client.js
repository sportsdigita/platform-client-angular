(function () {
    'use strict';
    angular.module('platform-client').service('labelClient', [
        '$http',
        '$q',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, $filter, tokenManager, batchRequestsService) {
            this.getLabels = function (deckId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/decks/' + deckId + '/labels', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getLabel = function (deckId, labelId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/decks/' + deckId + '/labels/' + labelId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createLabel = function (deckId, label) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/labels', label, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateLabel = function (deckId, labelId, label) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/labels/' + labelId, label, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateLabels = function (deckId, labels) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/decks/' + deckId + '/labels', labels, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteLabel = function (deckId, labelId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/decks/' + deckId + '/labels/' + labelId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteLabels = function (deckId, labelIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/decks/' + deckId + '/labels',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: labelIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
