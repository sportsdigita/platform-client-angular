(function () {
    'use strict';
    angular.module('platform-client').service('masterStyleComponentStylesClient', [
        '$http',
        '$q',
        'platformClientConfig',
        '$filter',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, $filter, tokenManager, batchRequestsService) {
            this.getMasterStyleComponentStyles = function (masterStyleId, filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/masterstyles/' + masterStyleId + '/componentstyles', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getMasterStyleComponentStyle = function (masterStyleId, componentStyleId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/masterstyles/' + masterStyleId + '/componentstyles/' + componentStyleId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createMasterStyleComponentStyle = function (masterStyleId, componentStyle) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/masterstyles/' + masterStyleId + '/componentstyles', componentStyle, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateMasterStyleComponentStyle = function (masterStyleId, componentStyleId, componentStyle) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/masterstyles/' + masterStyleId + '/componentstyles/' + componentStyleId, componentStyle, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteMasterStyleComponentStyle = function (masterStyleId, componentStyleId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/v1/masterstyles/' + masterStyleId + '/componentstyles/' + componentStyleId, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
