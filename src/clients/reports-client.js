(function () {
    'use strict';
    angular.module('platform-client').service('reportsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        'batchRequestsService',
        function ($http, $q, config, tokenManager, batchRequestsService) {
            this.getReports = function (filter) {
                var deferred = $q.defer();

                batchRequestsService
                    .batchGetRequests(config.apiPrefix + '/v1/reports', filter)
                    .then(function (data) {
                        deferred.resolve(data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.getReport = function (reportId) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/reports/' + reportId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.createReport = function (report) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/reports', report, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.updateReport = function (reportId, report) {
                var deferred = $q.defer();

                $http
                    .put(config.apiPrefix + '/v1/reports/' + reportId, report, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteReport = function (reportId) {
                var deferred = $q.defer();

                $http
                    .delete(config.apiPrefix + '/reports/' + reportId, { headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.deleteReports = function (reportIds) {
                var deferred = $q.defer();

                $http({
                    url: config.apiPrefix + '/v1/reports',
                    headers: { Authorization: tokenManager.getAuthorizationHeader(), 'Content-Type': 'application/json;charset=utf-8' },
                    data: reportIds,
                    method: 'DELETE'
                })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };

            this.runReport = function (reportId, filter) {
                var deferred = $q.defer();

                $http
                    .get(config.apiPrefix + '/v1/reports/' + reportId + '/data', {
                        params: filter,
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
