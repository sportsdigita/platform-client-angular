(function () {
    'use strict';
    angular.module('platform-client').service('deckSlideAssociationsClient', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            this.getDeckSlideAssociations = function (deckId, slideIds) {
                var deferred = $q.defer();

                $http
                    .post(config.apiPrefix + '/v1/decks/' + deckId + '/slides/associations', slideIds, {
                        headers: { Authorization: tokenManager.getAuthorizationHeader() }
                    })
                    .then(function (data) {
                        deferred.resolve(data.data);
                    })
                    .catch(function (response, status) {
                        deferred.reject({ response: response, status: status });
                    });

                return deferred.promise;
            };
        }
    ]);
})();
