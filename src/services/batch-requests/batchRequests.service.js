(function () {
    'use strict';

    angular.module('platform-client').service('batchRequestsService', [
        '$http',
        '$q',
        'platformClientConfig',
        'platformClientTokenManager',
        function ($http, $q, config, tokenManager) {
            var self = this;
            this.requests = [];

            this.cancelRequests = function () {
                if (self.requests.length > 0) {
                    angular.forEach(self.requests, function (request) {
                        request.resolve(new Error('The user cancelled requests.'));
                    });
                }
            };

            this.batchGetRequests = function (route, filter) {
                var deferred = $q.defer();
                var canceller = $q.defer();

                var results = [];
                var reqFilter = filter ? angular.copy(filter) : {};
                var amountToGet = reqFilter.take ? reqFilter.take : reqFilter.filter && reqFilter.filter.take ? reqFilter.filter.take : null;

                if (!reqFilter.filter && (!reqFilter.take || reqFilter.take > 100)) {
                    reqFilter.take = 100;
                }
                if (reqFilter.filter && (!reqFilter.filter.take || reqFilter.filter.take > 100)) {
                    reqFilter.filter.take = 100;
                }

                var makeAdditionalRequests = function () {
                    var promises = [];
                    var take = reqFilter.take ? reqFilter.take : reqFilter.filter.take;
                    for (var i = take; i < amountToGet; i += take) {
                        var nextFilter = angular.copy(reqFilter);
                        if (nextFilter.filter) {
                            nextFilter.filter.skip = i;
                        } else {
                            nextFilter.skip = i;
                        }
                        promises.push(
                            $http.get(route, {
                                params: nextFilter,
                                timeout: canceller.promise,
                                headers: { Authorization: tokenManager.getAuthorizationHeader() }
                            })
                        );
                    }

                    $q.all(promises)
                        .then(function (responses) {
                            responses.forEach(function (data) {
                                results = results.concat(data.data.results);
                            });
                            self.requests.splice(self.requests.indexOf(canceller), 1);
                            deferred.resolve({ results: results, total: amountToGet });
                        })
                        .catch(function (res, status) {
                            self.requests.splice(self.requests.indexOf(canceller), 1);
                            deferred.reject({ response: res, status: status });
                        });
                };

                $http
                    .get(route, { params: reqFilter, timeout: canceller.promise, headers: { Authorization: tokenManager.getAuthorizationHeader() } })
                    .then(function (data) {
                        results = data.data.results;
                        if (amountToGet === null) {
                            amountToGet = data.data.total;
                        }

                        if (results.length < amountToGet && results.length != data.data.total) {
                            makeAdditionalRequests();
                        } else {
                            self.requests.splice(self.requests.indexOf(canceller), 1);
                            deferred.resolve(data.data);
                        }
                    })
                    .catch(function (response, status) {
                        self.requests.splice(self.requests.indexOf(canceller), 1);
                        deferred.reject({ response: response, status: status });
                    });

                self.requests.push(canceller);

                return deferred.promise;
            };
        }
    ]);
})();
