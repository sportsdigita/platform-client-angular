(function () {
    'use strict';

    angular.module('platform-client').factory('memoryStorageFactory', [
        function memoryStorageFactory() {
            var memoryStorage = {};

            return {
                setItem: function (key, value) {
                    memoryStorage[key] = value;
                },

                getItem: function (key) {
                    return memoryStorage[key] ? memoryStorage[key] : null;
                },

                removeItem: function (key) {
                    delete memoryStorage[key];
                }
            };
        }
    ]);
})();
