(function () {
    'use strict';
    angular.module('platform-client').service('clientUtil', [
        'platformClientConfig',
        function (config) {
            this.getApiPrefix = function (options) {
                return options && options.apiPrefix ? options.apiPrefix : config.apiPrefix;
            };
        }
    ]);
})();
