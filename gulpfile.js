import gulp from 'gulp';
import gutil from 'gulp-util';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat-util';
import clean from 'gulp-clean';
import runSequence from 'run-sequence';

function runClean() {
    return gulp.src(['dist'], { read: false, allowEmpty: true })
        .pipe(clean());
}

var buildFiles = ['./src/*.js', './src/factories/*.js', './src/clients/*.js', './src/services/**/*.js', './src/utils/*.js'];

function buildMin() {
    return gulp.src(buildFiles)
        .pipe(concat('platform-client-angular.min.js'))
        .pipe(uglify())
        // .on('error', function(err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('dist'));
}

function buildRegular() {
    return gulp.src(buildFiles)
        .pipe(concat('platform-client-angular.js'))
        .pipe(gulp.dest('dist'));
}

export const build = gulp.series(runClean, buildMin, buildRegular);